package com.rokas.trafihw;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.AndroidTestCase;

import com.rokas.trafihw.models.Coordinate;
import com.rokas.trafihw.models.MapMarker;
import com.rokas.trafihw.utils.ImageGenerator;

/**
 * Created by Rokas on 2015-03-15.
 */
public class ImageGeneratorTest extends AndroidTestCase {

    public void testGetBitmapFromUrl() throws Exception {
        assertNull(ImageGenerator.getBitmapFromUrl(getContext(), null));
        assertNull(ImageGenerator.getBitmapFromUrl(getContext(), "test_url"));
        assertNull(ImageGenerator.getBitmapFromUrl(null, null));
        assertNull(ImageGenerator.getBitmapFromUrl(null, null));
        assertNull(ImageGenerator.getBitmapFromUrl(getContext(), "https://www.google.lt/"));
        assertNotNull(ImageGenerator.getBitmapFromUrl(getContext(), "https://www.google.lt/images/nav_logo195.png"));
    }

    public void testdpToPx() throws Exception {
        assertTrue(ImageGenerator.dpToPx(0) == 0);
        assertTrue(ImageGenerator.dpToPx(-1) == 0);
        assertTrue(ImageGenerator.dpToPx(10) > 0);
    }

    public void testgenerateTooltip() throws Exception {
        final Bitmap overlay = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.ic_current_location_off);
        assertNull(ImageGenerator.generateTooltipBitmap(null, overlay, 100, "ffffff"));
        assertNull(ImageGenerator.generateTooltipBitmap(null, overlay, 100, null));
        assertNull(ImageGenerator.generateTooltipBitmap(getContext(), null, 100, "ffffff"));
        assertNull(ImageGenerator.generateTooltipBitmap(getContext(), overlay, 100, "ffffff"));
    }

    public void testgenerateVehicleBitmap() throws Exception {
        assertNotNull(ImageGenerator.generateVehicleBitmap(getContext(), "https://www.google.lt/images/nav_logo195.png", "ffffff", 1));
        assertNull(ImageGenerator.generateVehicleBitmap(null, "https://www.google.lt/images/nav_logo195.png", "ffffff", 1));
        assertNull(ImageGenerator.generateVehicleBitmap(getContext(), null, "ffffff", 1));
        assertNotNull(ImageGenerator.generateVehicleBitmap(getContext(), "https://www.google.lt/images/nav_logo195.png", null, 1));
        assertNull(ImageGenerator.generateVehicleBitmap(getContext(), "https://www.google.lt", "ffffff", 1));
    }

    public void testConstructUrl() throws Exception {
        MapMarker mapMarker = new MapMarker();
        mapMarker.vehicleId = "1";
        mapMarker.type = 1;
        mapMarker.angle = 10f;
        mapMarker.color = "aabbcc";
        mapMarker.coordinate = new Coordinate();
        mapMarker.coordinate.lat = 55f;
        mapMarker.coordinate.lng = 55f;
        mapMarker.mapIcon = "a.png";
        mapMarker.mapIconSize = 10;
        mapMarker.toolTipTitleText = "test";

        assertEquals("http://cdn.trafi.com/icon.ashx?style=androidv2&size=30&src=a.png&cl=ffffff", ImageGenerator.constructImageUrl(mapMarker));
    }
}
