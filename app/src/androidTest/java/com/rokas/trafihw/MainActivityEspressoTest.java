package com.rokas.espressotest;

import android.support.test.espresso.assertion.ViewAssertions;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.rokas.trafihw.MainActivity;
import com.rokas.trafihw.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.doubleClick;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
public class MainActivityEspressoTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityEspressoTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }


    public void testMap() {
        onView(withId(R.id.map)).check(ViewAssertions.matches(isDisplayed()));
    }

    public void testFabVehicles() {
        onView(withId(R.id.fab_vehicles)).check(ViewAssertions.matches(isDisplayed()));
    }

    public void testFabLocation() {
        onView(withId(R.id.fab_location)).check(ViewAssertions.matches(isDisplayed()));
    }

    public void testFabVehiclesButtonClick() {
        onView(withId(R.id.fab_vehicles)).perform(click());
        sleep(2000);
        onView(withId(R.id.fab_vehicles)).perform(click());
    }

    public void testFabLocationButtonClick() {
        onView(withId(R.id.fab_location)).perform(click());
        sleep(2000);
        onView(withId(R.id.fab_location)).perform(click());
    }

    public void testMapNavigation() {
        onView(withId(R.id.map)).perform(swipeLeft());
        sleep(2000);
        onView(withId(R.id.map)).perform(swipeLeft());
        sleep(2000);
        onView(withId(R.id.map)).perform(doubleClick());
        sleep(2000);
        onView(withId(R.id.map)).perform(swipeLeft());
        sleep(2000);
        onView(withId(R.id.map)).perform(swipeLeft());
    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}