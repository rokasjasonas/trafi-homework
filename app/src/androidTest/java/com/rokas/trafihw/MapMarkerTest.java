package com.rokas.trafihw;

import android.test.AndroidTestCase;

import com.rokas.trafihw.models.Coordinate;
import com.rokas.trafihw.models.MapMarker;


public class MapMarkerTest extends AndroidTestCase {

    public void testMapMarkerEqual() throws Exception {
        MapMarker a = new MapMarker();
        a.vehicleId = "1";
        a.type = 1;
        a.angle = 10f;
        a.color = "aabbcc";
        a.coordinate = new Coordinate();
        a.coordinate.lat = 55f;
        a.coordinate.lng = 55f;
        a.mapIcon = "a.png";
        a.mapIconSize = 10;
        a.toolTipTitleText = "test";

        MapMarker b = new MapMarker();
        b.vehicleId = "1";
        b.type = 1;
        b.angle = 10f;
        b.color = "aabbcc";
        b.coordinate = new Coordinate();
        b.coordinate.lat = 55f;
        b.coordinate.lng = 55f;
        b.mapIcon = "a.png";
        b.mapIconSize = 10;
        b.toolTipTitleText = "test";

        assertTrue(a.equals(b));

        b.vehicleId = "2";
        assertFalse(a.equals(b));
    }
}