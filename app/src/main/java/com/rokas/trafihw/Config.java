package com.rokas.trafihw;

/**
 * Created by Rokas on 2015-03-11.
 */
public class Config {
    public static final String ENDPOINT = "http://api-lithuania-dev.trafi.com/api/v2";
    public static final String IMAGES_URL = "http://cdn.trafi.com/icon.ashx?" +
            "style=androidv2" +
            "&size=%s" +
            "&src=%s" +
            "&cl=ffffff";
}
