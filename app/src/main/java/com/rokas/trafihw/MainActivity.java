package com.rokas.trafihw;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;
import com.rokas.trafihw.models.Coordinate;
import com.rokas.trafihw.models.LatLngInterpolator;
import com.rokas.trafihw.models.MapMarker;
import com.rokas.trafihw.ui.widgets.AnimatedTooltip;
import com.rokas.trafihw.utils.ImageGenerator;
import com.rokas.trafihw.utils.MapUtils;
import com.rokas.trafihw.utils.Network;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;


public class MainActivity extends Activity implements OnMapReadyCallback {
    private static final String VEHICLES = "vehicles";
    private static final String LOCATION = "location";
    private static final float DEFAULT_ZOOM = 13;
    private static final long VEHICLES_REFRESH_INTERVAL = 5000;

    @InjectView(R.id.ll_tooltip)
    AnimatedTooltip llTooltip;

    @InjectView(R.id.tv_tooltip)
    TextView tvTooltip;

    @InjectView(R.id.iv_tooltip)
    ImageView ivTooltip;

    @InjectView(R.id.fab_vehicles)
    FloatingActionButton fabVehicles;

    @InjectView(R.id.fab_location)
    FloatingActionButton fabLocation;

    MapFragment mapFragment;

    private SharedPreferences sharedPreferences;

    private boolean vehiclesState;
    private boolean locationState;

    private LatLngBounds bounds;
    private Handler handler;
    private String city = "";
    private Map<Marker, MapMarker> currentMapMarkerMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        handler = new Handler();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        llTooltip.hide(false, null);

        vehiclesState = getButtonState(VEHICLES);
        locationState = getButtonState(LOCATION);

        updateLocationButton();
        updateVehiclesButton();

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void updateButton(FloatingActionButton floatingActionButton, boolean buttonState, int on, int off) {
        floatingActionButton.setImageResource(buttonState ? on : off);
    }

    private void updateLocationButton() {
        updateButton(fabLocation, locationState, R.drawable.ic_current_location_on, R.drawable.ic_current_location_off);
    }

    private void updateVehiclesButton() {
        updateButton(fabVehicles, vehiclesState, R.drawable.ic_vehicles_on, R.drawable.ic_vehicles_off);
    }

    private void saveButtonState(String key, boolean isSelected) {
        sharedPreferences.edit().putBoolean(key, isSelected).apply();
    }

    private boolean getButtonState(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    @OnClick(R.id.fab_vehicles)
    public void toggleVehicles(View view) {
        vehiclesState = !vehiclesState;
        saveButtonState(VEHICLES, vehiclesState);
        updateVehiclesButton();
        setVehiclesUpdate();
    }

    @OnClick(R.id.fab_location)
    public void toggleLocation(View view) {
        locationState = !locationState;
        saveButtonState(LOCATION, locationState);
        updateLocationButton();
        setMyLocationSettings();
    }

    private void setVehiclesUpdate() {
        handler.removeCallbacksAndMessages(null);

        if (vehiclesState) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (bounds != null) {
                        final LatLng northeast = bounds.northeast;
                        final LatLng southwest = bounds.southwest;

                        Network.get().requestMarkers(city,
                                southwest.latitude, southwest.longitude,
                                northeast.latitude, northeast.longitude,
                                new Network.OnRespondListener() {
                                    @Override
                                    public void onRespond(List<MapMarker> result) {
                                        if (result == null) {
                                            return;
                                        }

                                        drawMapMarkers(result);
                                    }
                                });
                    }

                    handler.postDelayed(this, VEHICLES_REFRESH_INTERVAL);
                }
            });
        } else {
            mapFragment.getMap().clear();
        }
    }

    private void drawMapMarkers(List<MapMarker> mapMarkers) {
        final GoogleMap map = mapFragment.getMap();

        // remove unused
        MapUtils.removeNotPresentMarkers(currentMapMarkerMap, mapMarkers, handler);

        for (final MapMarker mapMarker : mapMarkers) {
            if (mapMarker.type != 2) {
                continue;
            }

            final String url = ImageGenerator.constructImageUrl(mapMarker);
            final Bitmap mapIcon = ImageGenerator.generateVehicleBitmap(MainActivity.this, url, mapMarker.color, mapMarker.angle);

            if (mapIcon == null) {
                continue;
            }

            final Coordinate coordinate = mapMarker.coordinate;
            final LatLng latLng = new LatLng(coordinate.lat, coordinate.lng);

            if (currentMapMarkerMap.containsValue(mapMarker)) {
                updateMarkerOnMap(mapMarker, latLng);
            } else {
                addNewMarkerOnMap(map, mapMarker, mapIcon, latLng);
            }
        }
    }

    private void addNewMarkerOnMap(final GoogleMap map, final MapMarker mapMarker, Bitmap mapIcon, LatLng latLng) {
        final MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromBitmap(mapIcon));
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (map != null) {
                    Marker marker = map.addMarker(options);
                    currentMapMarkerMap.put(marker, mapMarker);
                }
            }
        });
    }

    private void updateMarkerOnMap(MapMarker mapMarker, final LatLng latLng) {
        final Marker marker = MapUtils.getMarker(currentMapMarkerMap, mapMarker);
        handler.post(new Runnable() {
            @Override
            public void run() {
                MapUtils.animateMarker(marker, latLng, new LatLngInterpolator.Linear());
            }
        });
        currentMapMarkerMap.put(marker, mapMarker);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        setMyLocationSettings();
        setMapBoundListener();
        setVehiclesUpdate();
        initMarkerClickListener();
        initTooltipListener();
    }

    private void initTooltipListener() {
        mapFragment.getMap().setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                llTooltip.hide(true, new AnimatedTooltip.Callback() {
                    @Override
                    public void onProgress(double progress) {
                    }

                    @Override
                    public void onEnd() {
                        llTooltip.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    private void initMarkerClickListener() {
        mapFragment.getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                final MapMarker mapMarker = currentMapMarkerMap.get(marker);
                Picasso.with(MainActivity.this).load(ImageGenerator.constructImageUrl(mapMarker)).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        llTooltip.setVisibility(View.VISIBLE);
                        llTooltip.reveal(true, null);
                        ivTooltip.setImageBitmap(ImageGenerator.generateTooltipBitmap(MainActivity.this, bitmap, mapMarker.mapIconSize, mapMarker.color));
                        tvTooltip.setText(mapMarker.toolTipTitleText);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });

                return true;
            }
        });
    }

    private void setMapBoundListener() {
        final GoogleMap map = mapFragment.getMap();
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(final CameraPosition cameraPosition) {
                updateCurrentCity(cameraPosition, map);
            }
        });
    }

    private void updateCurrentCity(final CameraPosition cameraPosition, final GoogleMap map) {
        bounds = map.getProjection().getVisibleRegion().latLngBounds;
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Geocoder geoCoder = new Geocoder(MainActivity.this, Locale.getDefault());
                try {
                    final LatLng target = cameraPosition.target;
                    List<Address> addressList = geoCoder.getFromLocation(target.latitude, target.longitude, 1);
                    if (addressList.size() > 0) {
                        final Address address = addressList.get(0);
                        city = address.getLocality();
                        if (city != null) {
                            city = city.toLowerCase().replace(" ", "+");
                        } else {
                            city = "";
                        }
                    }
                } catch (Exception e) {
                    city = "";
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void setMyLocationSettings() {
        final GoogleMap map = mapFragment.getMap();
        map.setMyLocationEnabled(locationState);
        if (locationState) {

            if (map.getMyLocation() == null) {
                map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        moveToLocation(location);
                        map.setOnMyLocationChangeListener(null);
                    }
                });
            } else {
                moveToLocation(map.getMyLocation());
            }
        } else {
            map.setOnMyLocationChangeListener(null);
        }
    }

    private void moveToLocation(Location location) {
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mapFragment.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }

}
