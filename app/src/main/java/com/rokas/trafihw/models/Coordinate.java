package com.rokas.trafihw.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rokas on 2015-03-01.
 */
public class Coordinate {
    @SerializedName("Lat")
    public float lat;

    @SerializedName("Lng")
    public float lng;
}
