package com.rokas.trafihw.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rokas on 2015-03-01.
 */
public class MapMarker {
    @SerializedName("Coordinate")
    public Coordinate coordinate;

    @SerializedName("Type")
    public int type;

    @SerializedName("MapIcon")
    public String mapIcon;

    @SerializedName("MapIconSize")
    public int mapIconSize;

    @SerializedName("Color")
    public String color;

    @SerializedName("Angle")
    public float angle;

    @SerializedName("TooltipTitleText")
    public String toolTipTitleText;

    @SerializedName("VehicleId")
    public String vehicleId;

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MapMarker)) {
            return false;
        }
        MapMarker other = (MapMarker) o;
        return vehicleId.equals(other.vehicleId);
    }
}
