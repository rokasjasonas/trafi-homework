package com.rokas.trafihw.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rokas on 2015-03-09.
 */
public class MapMarkersWrapper {
    @SerializedName("MapMarkers")
    public
    List<MapMarker> mapMarkers;
}
