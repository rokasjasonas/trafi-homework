package com.rokas.trafihw.ui.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;
import com.rokas.trafihw.R;

public class AnimatedTooltip extends LinearLayout implements SpringListener {
    private final Spring mTransitionSpring;
    private final float tooltipHeight;
    private final float tooltipTopPadding;
    private Callback mCallback;

    public interface Callback {
        void onProgress(double progress);

        void onEnd();
    }

    public AnimatedTooltip(Context context) {
        this(context, null);
    }

    public AnimatedTooltip(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimatedTooltip(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        final Resources resources = getResources();
        tooltipTopPadding = resources.getDimension(R.dimen.tooltip_top_padding);
        tooltipHeight = getResources().getDimension(R.dimen.tooltip_height);
        final SpringSystem mSpringSystem = SpringSystem.create();
        mTransitionSpring = mSpringSystem.createSpring();
        setClickable(true);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mTransitionSpring.setCurrentValue(1).setAtRest();
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
        setBackgroundColor(Color.WHITE);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mTransitionSpring.addListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mTransitionSpring.removeListener(this);
    }

    public void reveal(boolean animated, Callback callback) {
        if (animated) {
            mTransitionSpring.setEndValue(0);
        } else {
            mTransitionSpring.setCurrentValue(0).setAtRest();
        }
        mCallback = callback;
    }

    public void hide(boolean animated, Callback callback) {
        if (animated) {
            mTransitionSpring.setEndValue(1);
        } else {
            mTransitionSpring.setCurrentValue(1).setAtRest();
        }
        mCallback = callback;
    }

    @Override
    public void onSpringUpdate(Spring spring) {
        double val = spring.getCurrentValue();
        float y = (float) SpringUtil.mapValueFromRangeToRange(val, 0, 1, -tooltipTopPadding, -tooltipHeight);
        setTranslationY(y);
        if (mCallback != null) {
            mCallback.onProgress(spring.getCurrentValue());
        }
    }

    @Override
    public void onSpringAtRest(Spring spring) {
        if (mCallback != null) {
            mCallback.onEnd();
        }
    }

    @Override
    public void onSpringActivate(Spring spring) {
    }

    @Override
    public void onSpringEndStateChange(Spring spring) {
    }
}