package com.rokas.trafihw.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;

import com.rokas.trafihw.Config;
import com.rokas.trafihw.R;
import com.rokas.trafihw.models.MapMarker;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by Rokas on 2015-03-04.
 */
public class ImageGenerator {
    public static Bitmap generateVehicleBitmap(Context context, String url, String color, float angle) {
        if (context == null || url == null) {
            return null;
        }

        final Bitmap overlay = getBitmapFromUrl(context, url);
        if (overlay == null) {
            return null;
        }

        if (color == null) {
            color = "ffffff";
        }

        final Bitmap icVehicleBg = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_vehicle_bg);

        final Bitmap bmp = Bitmap.createBitmap(icVehicleBg.getWidth(), icVehicleBg.getWidth(), Bitmap.Config.ARGB_8888);

        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

        final Canvas canvas = new Canvas(bmp);
        final int centerX = canvas.getWidth() / 2;
        final int centerY = canvas.getHeight() / 2;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle, centerX, centerY);
        canvas.drawBitmap(icVehicleBg, matrix, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        p.setColor(Color.parseColor("#" + color));
        p.setMaskFilter(new BlurMaskFilter(15, BlurMaskFilter.Blur.NORMAL));
        canvas.drawPaint(p);

        final int left = centerX - overlay.getWidth() / 2;
        final int top = centerY - overlay.getHeight() / 2;
        canvas.drawBitmap(overlay, left, top, null);
        return bmp;
    }

    public static Bitmap getBitmapFromUrl(Context context, String url) {
        try {
            return Picasso.with(context).load(url).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap generateTooltipBitmap(Context context, Bitmap overlay, int sizeDp, String color) {
        if (context == null || overlay == null) {
            return null;
        }

        if (color == null) {
            color = "ffffff";
        }

        final int size = dpToPx(sizeDp);
        Bitmap bmp = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.parseColor("#" + color));

        final int roundSize = context.getResources().getDimensionPixelSize(R.dimen.rectangle_corner_radius);
        canvas.drawRoundRect(new RectF(0, 0, size, size), roundSize, roundSize, p);

        final int left = canvas.getWidth() / 2 - overlay.getWidth() / 2;
        final int top = canvas.getHeight() / 2 - overlay.getHeight() / 2;
        canvas.drawBitmap(overlay, left, top, null);
        return bmp;
    }

    public static int dpToPx(int dp) {
        if (dp < 0) {
            return 0;
        }

        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static String constructImageUrl(MapMarker mapMarker) {
        return String.format(Config.IMAGES_URL,
                ImageGenerator.dpToPx(mapMarker.mapIconSize),
                mapMarker.mapIcon);
    }
}
