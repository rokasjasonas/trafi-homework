package com.rokas.trafihw.utils;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.os.Handler;
import android.util.Property;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.rokas.trafihw.models.LatLngInterpolator;
import com.rokas.trafihw.models.MapMarker;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Rokas on 2015-03-15.
 */
public class MapUtils {
    public static Marker getMarker(Map<Marker, MapMarker>  currentMapMarkerMap, MapMarker mapMarker) {
        for (Map.Entry<Marker, MapMarker> entry : currentMapMarkerMap.entrySet()) {
            if (entry.getValue().equals(mapMarker)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static void removeNotPresentMarkers(Map<Marker, MapMarker>  currentMapMarkerMap, List<MapMarker> mapMarkers, Handler handler) {
        final Iterator<Map.Entry<Marker, MapMarker>> it = currentMapMarkerMap.entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry<Marker, MapMarker> entry = it.next();
            if (!mapMarkers.contains(entry.getValue())) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        entry.getKey().remove();
                    }
                });
                it.remove();
            }
        }
    }

    public static void animateMarker(Marker marker, LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {
        TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
            @Override
            public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                return latLngInterpolator.interpolate(fraction, startValue, endValue);
            }
        };
        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");
        ObjectAnimator animator = ObjectAnimator.ofObject(marker, property, typeEvaluator, finalPosition);
        animator.setDuration(3000);
        animator.start();
    }
}
