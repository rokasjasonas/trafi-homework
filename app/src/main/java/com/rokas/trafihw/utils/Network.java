package com.rokas.trafihw.utils;


import com.rokas.trafihw.Config;
import com.rokas.trafihw.models.MapMarker;
import com.rokas.trafihw.models.MapMarkersWrapper;

import java.util.List;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class Network {
    private static TrafiService service;
    private static Network singleton;

    private Network() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.ENDPOINT)
                .build();
        service = restAdapter.create(TrafiService.class);
    }

    public static Network get() {
        if (singleton == null) {
            singleton = new Network();
        }
        return singleton;
    }

    /**
     * Run this method from main thread
     */
    public void requestMarkers(final String userlocationId,
                               final double southWestLat,
                               final double southWestLng,
                               final double northEastLat,
                               final double northEastLng, final OnRespondListener eventListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                MapMarkersWrapper mapMarkers = null;

                try {
                    mapMarkers = service.getMapMarkers(userlocationId,
                            southWestLat, southWestLng,
                            northEastLat, northEastLng);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (eventListener != null) {
                    if (mapMarkers != null) {
                        eventListener.onRespond(mapMarkers.mapMarkers);
                    } else {
                        eventListener.onRespond(null);
                    }
                }
            }
        }).start();
    }

    public interface OnRespondListener {
        void onRespond(List<MapMarker> result);
    }

    public interface TrafiService {
        @GET("/mapObjects")
        MapMarkersWrapper getMapMarkers(@Query("userlocationId") String userlocationId,
                                        @Query("southWestLat") double southWestLat,
                                        @Query("southWestLng") double southWestLng,
                                        @Query("northEastLat") double northEastLat,
                                        @Query("northEastLng") double northEastLng);
    }
}
